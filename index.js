const btn = document.querySelectorAll(".tabs-title");
for (let i = 0; i < btn.length; i++) {
  btn[i].setAttribute("data-text", "text" + [i + 1]);
}
let content = document.querySelectorAll(".tabs-content li");
for (let i = 0; i < content.length; i++) {
  content[i].classList.add("text" + [i + 1]);
}

let list = document.querySelector(".tabs");

list.addEventListener("click", (event) => {
  let target = event.target;
  btnClick(target);
});

let button;
let textBlock;
function btnClick(currentButton) {
  let attrBtn = currentButton.getAttribute("data-text");
  let currentTextBlock = document.querySelector("." + attrBtn);
  document.querySelector(".active").classList.remove("active");
  document.querySelector(".active-text").classList.remove("active-text");
  if (button) {
    button.classList.remove("active");
    textBlock.classList.remove("active-text");
  }
  button = currentButton;
  textBlock = currentTextBlock;
  button.classList.add("active");
  textBlock.classList.add("active-text");
}
